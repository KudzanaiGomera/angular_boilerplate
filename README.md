# README #

* is built using angular 8
* make sure you have node js installed

### What is this repository for? ###

* This repo contains a angular 8 boilerplate
* angular 8

### How do I get set up? ###

* git clone https://KudzanaiGomera@bitbucket.org/KudzanaiGomera/angular_boilerplate.git
* cd angular_boilerplate
* cd frontend

### How to run? ###
* ng serve

### Contribution guidelines ###

* Writing tests -> Kudzanai Gomera
* Code review -> Junior and Sipho

### Who do I talk to? ###

* Kudzanai Gomera
* Katanga Dev Team